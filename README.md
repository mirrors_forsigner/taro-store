# taro-store

> State management for Taro

## Installation

```sh
yarn add taro-store
```

## Quick Start

```js
import Taro from '@tarojs/taro'
import { View, Text, Button } from '@tarojs/components'
import { createStore, observe } from 'taro-store'

const store = createStore({
  count: 1,
  increment() {
    store.count++
  },
  decrement() {
    store.count--
  },
  async asyncIncrement() {
    await new Promise(resolve => setTimeout(resolve, 1000))
    store.count++
  },
})

const App = () => (
  <View>
    <Text>{store.count}</Text>
    <Button onClick={store.decrement}>-</Button>
    <Button onClick={store.increment}>+</Button>
    <Button onClick={store.asyncIncrement}>async+</Button>
  </View>
))

export default observe(App)

```

## License

[MIT License](https://github.com/forsigner/taro-store/blob/master/LICENSE)
