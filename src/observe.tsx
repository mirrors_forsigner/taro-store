import { observe as run, unobserve } from 'dahlia-observable';

export function observe(component: any) {
  let render: any;
  const target = component.prototype;
  const originComponentWillMount = target.componentWillMount;
  const originComponentWillUnmount = target.componentWillUnmount;

  target.componentWillMount = function() {
    render = run(this.render, {
      scheduler: () => {
        this.setState({});
      },
      lazy: true,
    });

    originComponentWillMount && originComponentWillMount.call(this);
  };

  target.componentWillUnmount = function() {
    unobserve(render);
    originComponentWillUnmount && originComponentWillUnmount.call(this);
  };

  return component;
}
